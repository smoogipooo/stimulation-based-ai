﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Stimulation_Based_AI.Actions;
using Stimulation_Based_AI.Helpers;
using Stimulation_Based_AI.Input;
using Stimulation_Based_AI.Rulesets;
using Stimulation_Based_AI.Rulesets.Maze;

namespace Stimulation_Based_AI
{
    public class GameBase : Game
    {
        private const int UPDATE_FPS = 10;

        private static GraphicsDeviceManager graphics;

        internal static SpriteBatch SpriteBatch;
        internal static Ruleset CurrentRuleset;
        internal static Texture2D WhitePixel;
        internal static Random Random = new Random();

        public GameBase()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 640;
            graphics.PreferredBackBufferHeight = 480;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();
            IsMouseVisible = true;
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
            WhitePixel = Texture2D.FromStream(GraphicsDevice, ResourceHelper.GetResourceStream("WhitePixel.jpg"));
        }

        protected override void Update(GameTime gameTime)
        {

            if (CurrentRuleset == null)
                ChangeMode(AIModes.Maze);

            InputManager.Update();
            if (gameTime.TotalGameTime.Milliseconds / 1000f >= 1f / UPDATE_FPS)
            {
                CurrentRuleset.Update();
                gameTime.TotalGameTime = TimeSpan.Zero;
            }

            base.Update(gameTime);
        }

        private static void ChangeMode(AIModes mode)
        {
            if (CurrentRuleset != null)
                CurrentRuleset.Dispose();

            switch (mode)
            {
                case AIModes.Maze:
                    CurrentRuleset = new RulesetMaze();
                    break;
            }

            CurrentRuleset.Initialize();
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Gainsboro);

            SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            CurrentRuleset.Draw();
            SpriteBatch.End();

            base.Draw(gameTime);
        }

        internal enum AIModes
        {
            Maze
        }
    }
}
