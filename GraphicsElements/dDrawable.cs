﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Stimulation_Based_AI.GraphicsElements
{
    class dDrawable
    {
        internal Texture2D Texture;

        internal Vector2 Origin
        {
            get
            {
                Vector2 midPt = new Vector2(Size.X / 2f, Size.Y / 2f);

                Vector2 p = new Vector2(Texture.Width, Texture.Height);

                if ((TextureOrigin & Origins.Top) > 0)
                    p.Y -= midPt.Y;
                if ((TextureOrigin & Origins.Bottom) > 0)
                    p.Y += midPt.Y;
                if ((TextureOrigin & Origins.Left) > 0)
                    p.X -= midPt.X;
                if ((TextureOrigin & Origins.Right) > 0)
                    p.X += midPt.X;

                return p;
            }
        }

        internal Vector2 VectorScale = Vector2.One;

        internal Vector2 Position = Vector2.Zero;

        private Vector2 size;
        internal Vector2 Size
        {
            get 
            { 
                return new Vector2(size.X * VectorScale.X, size.Y * VectorScale.Y); 
            }
            set
            {
                size = value;
            }
        }

        internal Rectangle DrawRectangle
        {
            get { return new Rectangle((int)Position.X, (int)Position.Y, (int)Size.X, (int)Size.Y); }
        }

        internal Color Colour = Color.White;
        private Origins TextureOrigin = Origins.Top | Origins.Left;

        internal string Source;

        public dDrawable(Texture2D texture)
        {
            Texture = texture;
            Source = texture.Name;
        }

        public dDrawable(Texture2D texture, Vector2 position)
            : this(texture)
        {
            Position = position;
        }

        public dDrawable(Texture2D texture, Vector2 position, Vector2 size)
            : this(texture, position)
        {
            Size = size;
        }

        public dDrawable(Texture2D texture, Vector2 position, Vector2 size, Origins origin)
            : this(texture, position, size)
        {
            TextureOrigin = origin;
        }

        public dDrawable(Texture2D texture, Vector2 position, Vector2 size, Origins origin, Color colour)
            : this(texture, position, size, origin)
        {
            Colour = colour;
        }

        [Flags]
        internal enum Origins
        {
            Top,
            Bottom,
            Left,
            Right,
            Center
        }
    }
}
