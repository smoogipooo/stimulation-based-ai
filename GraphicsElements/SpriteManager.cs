﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Stimulation_Based_AI.GraphicsElements
{
    class SpriteManager
    {
        private List<dDrawable> drawables = new List<dDrawable>();

        internal void Draw()
        {
            foreach (dDrawable d in drawables)
                GameBase.SpriteBatch.Draw(d.Texture, d.DrawRectangle, null, d.Colour, 0f, d.Origin, Microsoft.Xna.Framework.Graphics.SpriteEffects.None, 1.0f);
        }

        internal void Add(dDrawable sprite)
        {
            lock(drawables)
                drawables.Add(sprite);
        }

        internal void Remove(dDrawable sprite)
        {
            lock (drawables)
                drawables.Remove(sprite);
        }
    }
}
