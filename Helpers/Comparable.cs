﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Stimulation_Based_AI.Actions;
using Stimulation_Based_AI.GraphicsElements;
using Stimulation_Based_AI.Rulesets;
using Stimulation_Based_AI.Rulesets.Maze;

namespace Stimulation_Based_AI.Helpers
{
    static class DistanceHelpers
    {
        internal static float DistanceTo(this float value1, float value2)
        {
            return (float)Math.Sqrt(Math.Pow(value1 - value2, 2));
        }

        internal static float DistanceTo(this Vector2 value1, Vector2 value2)
        {
            return (float)Math.Sqrt(Math.Pow(value1.X - value2.X, 2) + Math.Pow(value1.Y - value2.Y, 2));
        }
    }

    abstract class Comparable
    {
        internal abstract float CompareTo(Comparable other);
    }

    class ComparablePoint : Comparable
    {
        internal Vector2 Position;

        internal override float CompareTo(Comparable other)
        {
            ComparablePoint otherComparable = other as ComparablePoint;

            return otherComparable == null ? float.MinValue : Position.DistanceTo(otherComparable.Position);
        }

        internal ComparablePoint(Vector2 position)
        {
            Position = position;
        }
    }

    class ComparableMazeSquare : ComparablePoint
    {
        internal bool IsPassable = false;
        internal bool ContainsPlayer = false;
        internal bool IsEnd = false;
        internal dDrawable Texture;

        internal ComparableMazeSquare(Vector2 position)
            : base(position)
        { }
    }
}
