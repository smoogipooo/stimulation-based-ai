﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Stimulation_Based_AI.Actions;
using Stimulation_Based_AI.GraphicsElements;
using Stimulation_Based_AI.Helpers;
using Stimulation_Based_AI.Rulesets;
using Action = Stimulation_Based_AI.Actions.Action;

namespace Stimulation_Based_AI.Agents
{
    internal enum AgentState
    {
        Initializing,
        Running,
        Stopped
    }
    class PriorityAgent
    {
        private Ruleset ruleset;

        private List<KeyValuePair<Action, float>> previousActions = new List<KeyValuePair<Action, float>>();

        private Dictionary<string, Comparable> comparables
        {
            get
            {
                Dictionary<string, Comparable> data = new Dictionary<string, Comparable>();
                data.Add("Position", new ComparablePoint(Position));
                return data.Concat(ruleset.GetComparables()).ToDictionary(k => k.Key, k => k.Value);
            }
        }

        FileStream outFileStream;
        StreamWriter stdOut;

        internal Vector2 Position;
        internal AgentState State = AgentState.Initializing;

        internal virtual void Initialize()
        {
            outFileStream = new FileStream("actionList.txt", FileMode.Create);
            stdOut = new StreamWriter(outFileStream);
            stdOut.AutoFlush = true;
            ruleset = GameBase.CurrentRuleset;
        }

        internal virtual void StartAgent()
        {
            State = AgentState.Running;
        }

        internal virtual void StopAgent(bool success)
        {
            State = AgentState.Stopped;
        }

        internal virtual void Update()
        {
            if (State != AgentState.Running)
                return;

            if (previousActions.Count == 0)
            {
                performRandomAction();
                return;
            }

            int count = Enum.GetNames(typeof(ActionType)).Count();

            Action nextAction = null;
            float nextEvaluation = float.MinValue;
            float closestComparison = float.MinValue;
            float closestEvaluation = float.MinValue;
            Action closestAction = null;

            Dictionary<string, Comparable> currentComparables = comparables;
            for (int i = 0; i < count; i++)
            {
                ActionType newType = (ActionType)i;
                Action newAction = new Action(newType, new ActionData(Position, currentComparables));

                //Find the most relevant of the previously performed actions
                //and compare the new suggested action to it
                foreach (KeyValuePair<Action, float> kvp in previousActions)
                {
                    float comparison = newAction.CompareTo(kvp.Key);
                    if (comparison >= closestComparison)
                    {
                        closestComparison = comparison;
                        closestEvaluation = ruleset.Evaluate(kvp.Key);
                        closestAction = kvp.Key;
                    }
                }

                float eval = ruleset.Evaluate(newAction);

                //Accept evaluation only if previous action is deemed unreasonable
                //i.e. ruleset tells agent to evolve or previous action is not applicable
                if (eval > closestEvaluation && eval > nextEvaluation)
                {
                    nextEvaluation = eval;
                    nextAction = newAction;
                    nextAction.Comparer = Comparer.Ruleset;
                }
                else if (closestEvaluation > nextEvaluation)
                {
                    nextAction = closestAction;
                    nextEvaluation = closestEvaluation;
                    nextAction.Comparer = Comparer.Actions;
                }
            }

            //If agent has calculated an evaluation but doesn't like it
            //it will perform a random action to build up its priority queue
            if (nextEvaluation < 0)
                performRandomAction();
            else
                performAction(nextAction, nextEvaluation);
        }

        private void performRandomAction()
        {
            int count = Enum.GetNames(typeof(ActionType)).Count();
            Action randomAction = new Action((ActionType)GameBase.Random.Next(0, count), new ActionData(Position, comparables));
            randomAction.Comparer = Comparer.Ruleset;
            performAction(randomAction, ruleset.Evaluate(randomAction));
        }

        private void performAction(Action action, float evaluation)
        {
            action.Evaluation = evaluation;
            ruleset.Perform(action);
            const string lineFormat = "({0,5}) Action: {1,12}, Sender: {2,7}, Evaluation: {3,3}";
            stdOut.WriteLine(lineFormat, previousActions.Count, action.Type, action.Comparer, action.Evaluation);
            previousActions.Add(new KeyValuePair<Action, float>(action, evaluation));
        }

        internal virtual void Dispose()
        {
            
        }
    }
}
