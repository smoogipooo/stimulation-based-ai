﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Stimulation_Based_AI.Actions;
using Stimulation_Based_AI.Agents;
using Stimulation_Based_AI.GraphicsElements;
using Stimulation_Based_AI.Helpers;
using Action = Stimulation_Based_AI.Actions.Action;

namespace Stimulation_Based_AI.Rulesets
{
    abstract class Ruleset
    {
        protected PriorityAgent currentAgent = new PriorityAgent();

        internal virtual void Initialize()
        {
            currentAgent.Initialize();
        }

        internal virtual void Update()
        {
            currentAgent.Update();
        }

        internal abstract void Draw();

        internal abstract Dictionary<string, Comparable> GetComparables();

        /// <summary>
        /// Evaluates the action in context to the rest of the ruleset.
        /// </summary>
        /// <param name="action">The action to evaluate.</param>
        /// <returns>The expected evaluation of the action.</returns>
        internal abstract float Evaluate(Action action);

        /// <summary>
        /// Performs an action.
        /// </summary>
        /// <param name="action">The action to perform.</param>
        internal abstract void Perform(Action action);

        internal void Begin()
        {
            currentAgent.StartAgent();
        }

        internal virtual void Dispose()
        {
            currentAgent.Dispose();
        }
    }
}
