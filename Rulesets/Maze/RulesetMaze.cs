﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Xna.Framework;
using Stimulation_Based_AI.Actions;
using Stimulation_Based_AI.Agents;
using Stimulation_Based_AI.GraphicsElements;
using Stimulation_Based_AI.Helpers;
using Action = Stimulation_Based_AI.Actions.Action;

namespace Stimulation_Based_AI.Rulesets.Maze
{
    class RulesetMaze : Ruleset
    {
        internal const int BOARD_SIZE = 400;
        internal const int BOARD_X = 145;
        internal const int BOARD_Y = 65;

        private SpriteManager spriteManager = new SpriteManager();

        private List<ComparableMazeSquare> squares = new List<ComparableMazeSquare>();
        private List<Vector2> previousPositions = new List<Vector2>();


        internal override void Initialize()
        {
            base.Initialize();

            MapGen.Generate(squares);
            if (squares.Count == 0)
                return;
            currentAgent.Position = MapGen.StartPosition;
            for (int i = 0; i < squares.Count; i++)
                spriteManager.Add(squares[i].Texture);

            previousPositions.Add(currentAgent.Position);

            Begin();
        } 

        internal override void Draw()
        {
            spriteManager.Draw();
        }

        internal override void Update()
        {
            if (currentAgent.State == AgentState.Running
                && currentAgent.Position == squares.Last(s => s.IsEnd).Position)
            {
                currentAgent.StopAgent(true);
                return;
            }

            foreach (ComparableMazeSquare sq in squares)
            {
                sq.ContainsPlayer = currentAgent.Position == sq.Position;
                if (sq.IsEnd)
                    sq.Texture.Colour = sq.ContainsPlayer ? Color.Blue : Color.Red;
                else
                {
                    if (sq.IsPassable)
                        sq.Texture.Colour = sq.ContainsPlayer ? Color.Green : Color.White;
                    else
                        sq.Texture.Colour = Color.Black;
                }
            }
            
            base.Update();
        }

        internal override float Evaluate(Action action)
        {
            Vector2 newPosition = currentAgent.Position + getPositionChange(action.Type);

            if (newPosition.X < BOARD_X || newPosition.Y < BOARD_Y
                || newPosition.X >= BOARD_X + BOARD_SIZE || newPosition.X >= BOARD_X + BOARD_SIZE)
            {
                return -1;
            }

            if (newPosition == currentAgent.Position)
                return -2;

            if (newPosition == MapGen.EndSquare.Position)
                return float.MaxValue;

            foreach (ComparableMazeSquare square in squares)
            {
                if (newPosition == square.Position)
                {
                    if (square.IsPassable)
                        return 1f / (previousPositions.Count(v => v == newPosition) + 1) + 1 / newPosition.DistanceTo(MapGen.EndSquare.Position);

                    return -1;
                }
            }

            return 0.5f;
        }

        private Vector2 getPositionChange(ActionType actionType)
        {
            Vector2 positionChange = Vector2.Zero;
            switch (actionType)
            {
                case ActionType.MoveDown:
                    positionChange.Y = MapGen.SquareSize;
                    break;
                case ActionType.MoveRight:
                    positionChange.X = MapGen.SquareSize;
                    break;
                case ActionType.MoveUp:
                    positionChange.Y = -MapGen.SquareSize;
                    break;
                case ActionType.MoveLeft:
                    positionChange.X = -MapGen.SquareSize;
                    break;
            }

            return positionChange;
        }

        internal override void Perform(Action action)
        {
            Vector2 newPosition = currentAgent.Position + getPositionChange(action.Type);

            if (squares.Any(square => newPosition == square.Position && !square.IsPassable)
                || newPosition.X < BOARD_X || newPosition.Y < BOARD_Y
                || newPosition.X >= BOARD_X + BOARD_SIZE || newPosition.X >= BOARD_X + BOARD_SIZE)
            {
                return;
            }

            currentAgent.Position = newPosition;
            previousPositions.Add(newPosition);

            if (currentAgent.Position == MapGen.EndSquare.Position)
                currentAgent.StopAgent(true);
        }

        internal override Dictionary<string, Comparable> GetComparables()
        {
            Dictionary<string, Comparable> ret = new Dictionary<string, Comparable>();
            for (int i = 0; i < squares.Count; i++)
                ret.Add("Block" + i, squares[i]);
            return ret;
        }

        internal override void Dispose()
        {
            base.Dispose();
        }
    }
}
