﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Stimulation_Based_AI.GraphicsElements;
using Stimulation_Based_AI.Helpers;

namespace Stimulation_Based_AI.Rulesets.Maze
{
    class MapGen
    {
        internal static float SquareSize = 0;

        internal static Vector2 StartPosition = Vector2.Zero;
        internal static ComparableMazeSquare EndSquare = null;
        internal static string Filename;

        public static void Generate(List<ComparableMazeSquare> squaresList)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    Filename = ofd.FileName;

                    bool isStart = true;
                    int w = 0, h = 0;
                    using (StreamReader sR = new StreamReader(ofd.FileName))
                    {
                        while (sR.Peek() != -1)
                        {
                            string[] line = sR.ReadLine().Split(',');
                            if (isStart)
                            {
                                w = Convert.ToInt32(line[0]);
                                SquareSize = (float)RulesetMaze.BOARD_SIZE / w;
                                isStart = false;
                                continue;
                            }

                            for (int i = 0; i < w; i++)
                            {
                                Vector2 targetPos = new Vector2(RulesetMaze.BOARD_X + SquareSize * i, RulesetMaze.BOARD_Y + SquareSize * h);

                                if (i >= line.Length)
                                {
                                    squaresList.Add(new ComparableMazeSquare(targetPos)
                                    {
                                        Texture = new dDrawable(GameBase.WhitePixel, targetPos, new Vector2(SquareSize, SquareSize), dDrawable.Origins.Center, Color.Black)
                                    });
                                }
                                else
                                {
                                    ComparableMazeSquare sq = new ComparableMazeSquare(targetPos)
                                    {
                                        IsPassable = line[i] != "1",
                                        ContainsPlayer = line[i] == "o",
                                        IsEnd = line[i] == "x"
                                    };
                                    sq.Texture = new dDrawable(GameBase.WhitePixel, targetPos, new Vector2(SquareSize, SquareSize), dDrawable.Origins.Center
                                                                , sq.ContainsPlayer
                                                                ? Color.Green
                                                                : sq.IsPassable ? Color.White : Color.Black);

                                    if (sq.ContainsPlayer)
                                        StartPosition = sq.Position;
                                    if (sq.IsEnd)
                                        EndSquare = sq;
                                    squaresList.Add(sq);
                                }
                            }
                            h += 1;
                        }                    
                    }
                }
            }
        }
    }
}
