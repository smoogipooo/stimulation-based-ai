﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Stimulation_Based_AI.GraphicsElements;
using Stimulation_Based_AI.Helpers;

namespace Stimulation_Based_AI.Actions
{
    internal enum ActionType
    {
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight
    }

    internal enum Comparer
    {
        Ruleset,
        Actions
    }

    class ActionData
    {
        private Vector2 position;
        private Dictionary<string, Comparable> extraData;

        internal ActionData(Vector2 agentPosition, Dictionary<string, Comparable> data = null)
        {
            position = agentPosition;
            extraData = data;
        }

        internal float CompareTo(ActionData other)
        {
            float internalResult = position.DistanceTo(other.position);

            if (extraData != null)
            {
                foreach (KeyValuePair<string, Comparable> data in extraData)
                {
                    Comparable otherData;
                    if (other.extraData.TryGetValue(data.Key, out otherData))
                        internalResult += data.Value.CompareTo(otherData);
                }
            }

            return internalResult;
        }
    }

    class Action
    {
        public ActionType Type;
        private ActionData data;
        internal float Evaluation = float.MinValue;

        /// <summary>
        /// The object which initiated the agent to perform this action.
        /// </summary>
        internal Comparer Comparer { get; set; }

        internal Action(ActionType type, ActionData data)
        {
            Type = type;
            this.data = data;
        }

        internal float CompareTo(Action other)
        {
            return Evaluation.DistanceTo(other.Evaluation) + data.CompareTo(other.data);
        }

    }
}